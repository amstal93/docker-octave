GNU Octave Docker images
========================

This repository provides a Docker image for the latest stable release of
[GNU Octave]. The current stable version of Octave is 5.1.0.

## Usage

This image can run an Octave command or script with

    docker pull mtmiller/octave
    docker run mtmiller/octave octave --eval "ver"

The default command is `octave`, so an interactive Octave session can be
started with

    docker run -it mtmiller/octave

The base image is `ubuntu:bionic`.

## Examples

One of the intended uses of this image is as a base for continuous integration
(CI) and/or continuous delivery (CD) in Octave projects.

See
[mtmiller/octave-gitlab-ci-example](https://gitlab.com/mtmiller/octave-gitlab-ci-example)
for an example project using GitLab CI. See
[mtmiller/octave-travis-ci-example](https://github.com/mtmiller/octave-travis-ci-example)
for an example GitHub project using Travis CI. See
[mtmiller/octave-bitbucket-pipeline-example](https://bitbucket.org/mtmiller/octave-bitbucket-pipeline-example)
for an example Bitbucket project using Bitbucket Pipelines.

## About Octave

[GNU Octave] is a high-level interpreted language, primarily intended for
numerical computations. It provides capabilities for the numerical solution of
linear and nonlinear problems, and for performing other numerical experiments.
It also provides extensive graphics capabilities for data visualization and
manipulation. Octave is normally used through its interactive command line
interface, but it can also be used to write non-interactive programs. The
Octave language is quite similar to Matlab so that most programs are easily
portable.

## Get Involved

This Docker image is maintained and built at
[mtmiller/docker-octave on GitLab](https://gitlab.com/mtmiller/docker-octave).
The image is published at
[mtmiller/octave on Docker Hub](https://hub.docker.com/r/mtmiller/octave).

If you want to contribute and help make this Docker image better, please read
the [contribution guidelines](CONTRIBUTING.md).

## License

The scripts used to build this Docker image are licensed under a modified BSD
license. See [LICENSE.md](LICENSE.md) for the full license text.

[GNU Octave] is free software: you can redistribute it and/or modify it under
the terms of the [GNU General Public License][gpl], either version 3 or any
later version.

[GNU Octave]: https://www.octave.org/
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
